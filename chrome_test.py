import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import os

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Remote(
            command_executor=os.environ.get('SELENIUM_URL', 'http://127.0.0.1:4444/wd/hub'),
            options=webdriver.ChromeOptions()
        )

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("https://www.python.org")
        self.assertIn("Python", driver.title)


    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()